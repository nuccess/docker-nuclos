#!/bin/bash

echo "Shut down Nuclos..." >> $NUCLOS_HOME/logs/server.log

rm -f $NUCLOS_HOME/logs/server*

if [ -f $NUCLOS_HOME/bin/shutdown.sh ]; then
	$NUCLOS_HOME/bin/shutdown.sh
	timeout 8s tomcat-wait-for-termination.sh
fi

pkill -9 -f java
