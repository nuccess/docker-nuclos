#!/bin/bash

# Nuclos installer to use
NUCLOS_INSTALLER="$1"
echo "Install/update Nuclos $NUCLOS_INSTALLER: start..." >> $NUCLOS_HOME/logs/server.log

# Disable restart (keep-alive)
echo "nuclos-install.sh" > $NUCLOS_DIR/keep-alive.disabled

# Shutdown or kill Nuclos
nuclos-shutdown.sh

# Nuclos tomcat directory, remove old one before installation
TOMCAT_DIR=$(find $NUCLOS_HOME -iname "apache-tomcat-*" -type d)
if [ -d "$TOMCAT_DIR" ]; then
	rm -fdr "$TOMCAT_DIR"
fi

# Setup client only during install, not with every run
sed -i 's|CLIENT_CONFIG_SERVER_HOST|'"$CLIENT_CONFIG_SERVER_HOST"'|g' $NUCLOS_HOME/nuclos.xml

# Execute the installer in silent mode
$JAVA_HOME/bin/java -jar $NUCLOS_INSTALLER -s $NUCLOS_HOME/nuclos.xml 2>&1 >> $NUCLOS_HOME/logs/server.log
TOMCAT_DIR=$(find $NUCLOS_HOME -iname "apache-tomcat-*" -type d)

# replace static: MAXHEAP=-Xmx640m
sed -i -E 's/(.*MAXHEAP=-Xmx)[0-9]+[a-zA-Z](.*)/\1'"$CLIENT_CONFIG_MAX_HEAP"'\2/' $NUCLOS_HOME/client/nuclos.*
sed -i -E 's/:'"$NUCLOS_PORT"'\//:'"$DOCKER_NUCLOS_PORT"'\//g' $NUCLOS_HOME/client/nuclos.*

# make scripts runnable
chmod +x $NUCLOS_HOME/bin/*.sh
chmod +x $TOMCAT_DIR/bin/*.sh

# Backup properties for later config
rm -f $NUCLOS_DIR/server.properties-template
rm -f $NUCLOS_DIR/jdbc.properties-template
cp $NUCLOS_CONF/server.properties $NUCLOS_DIR/server.properties-template
cp $NUCLOS_CONF/jdbc.properties $NUCLOS_DIR/jdbc.properties-template

# Change Tomcat defaults, remove templates, manager, etc.
# Place a redirect in ROOT to Nuclos webclient.
# Backup config files for updating with parameters during run.
rm -fdr $(find $TOMCAT_DIR/webapps/* -maxdepth 0 ! -name ROOT -type d)
rm -f $(find $TOMCAT_DIR/webapps/ROOT -maxdepth 1 -type f)
echo "<%@ page session=\"false\" %><% response.sendRedirect(\"/webclient\"); %>" > $TOMCAT_DIR/webapps/ROOT/index.jsp
mv $TOMCAT_DIR/$NUCLOS_WEBCLIENT_CONF $TOMCAT_DIR/$NUCLOS_WEBCLIENT_CONF-template
mv $TOMCAT_DIR/$NUCLOS_TOMCAT_CONF $TOMCAT_DIR/$NUCLOS_TOMCAT_CONF-template
mv $NUCLOS_STARTUP $NUCLOS_STARTUP-template

# Configure Nuclos installation
nuclos-config.sh

# Publish Richclient if mounted
if [ -d "$NUCLOS_DIR/client" ]; then
  echo "Publish Richclient to $NUCLOS_DIR/client..." >> $NUCLOS_HOME/logs/server.log
  rsync -qtplr --del --perms --chmod=D777,F555 --ignore-missing-args $NUCLOS_HOME/client/* $NUCLOS_DIR/client/
fi

# Enable keep-alive again
rm -f $NUCLOS_DIR/keep-alive.disabled

echo "Install/update Nuclos $NUCLOS_INSTALLER: complete... wait for restart" >> $NUCLOS_HOME/logs/server.log
