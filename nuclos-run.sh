#!/bin/bash

# Remove old install flag
rm -f $NUCLOS_DIR/nuclos.install

if [ ! -d $NUCLOS_HOME/tomcat ] || [ -z "$(ls -A $NUCLOS_HOME/tomcat)" ]; then
	# Install Nuclos if not already done before (first container startup)
	NUCLOS_INSTALLER=$(find $NUCLOS_DIR/update/backup/ -iname "*installer*.jar" -type f)
	echo "Install Nuclos $NUCLOS_INSTALLER (Details can be found in the install log $NUCLOS_HOME/logs/nuclos-installer_*.log)"
	nuclos-install.sh "$NUCLOS_INSTALLER"
else
	# Configure Nuclos only (ENVs may have been changed)
	nuclos-config.sh
fi

# Create schema if not exist
export DB_SCHEMA=${DB_SCHEMA:-'nuclos'}
nuclos-db-cmd.sh "CREATE SCHEMA IF NOT EXISTS $DB_SCHEMA AUTHORIZATION ${POSTGRES_ENV_POSTGRES_USER:-${POSTGRES_USER}}" start-$DB_SCHEMA-schema.sql


SCRIPT_SHUTDOWN=0

# Install shutdown trap
function nucshutdown {
	SCRIPT_SHUTDOWN=1
	$NUCLOS_HOME/bin/shutdown.sh
}
trap nucshutdown EXIT

# Start function
function startAppServer {
	# Remove old logs, to identify the server start correctly
	rm -f $NUCLOS_HOME/logs/server*
	
	# Remove old tomcat logs
	TOMCAT_DIR=$(find $NUCLOS_HOME/tomcat -iname "apache-tomcat-*" -type d)
	rm -f "$TOMCAT_DIR/logs/*"
	
	# Start the server
	$NUCLOS_HOME/bin/startup.sh
	
	while [ ! -f $NUCLOS_HOME/logs/server.log ]; do
		sleep 0.4
	done

	# Create Docker Nuclet for import(update) after AutoDbSetup completed
	DOCKER_NUCLET="set search_path to $DB_SCHEMA;"
	DOCKER_NUCLET+=$'\n'
	for FILE in `find $NUCLOS_DIR -maxdepth 1 -name "*.sql" -type f | sort -n`; do
		FILENAME="${FILE##*/}"
		DOCKER_NUCLET+=$(cat $NUCLOS_DIR/$FILENAME)
		DOCKER_NUCLET+=$'\n'
	done
	
	# Wait for auto-db-setup
	nice -n 19 timeout 300s bash -c "until bash -c 'test -f /opt/nuclos/home/logs/server.log \
    && PID=\$(jps -l | grep org.apache.catalina.startup.Bootstrap | cut -d \" \" -f 1) \
    && test ! -z \$PID && CHKTIME=\$(date \"+%Y-%m-%dT%H:%M:%S\") && JSTACK=\$(cpulimit --limit=50 --foreground --quiet -- jstack \$PID) \
    && test \$(echo \$JSTACK | grep \"AutoDbSetup\" | wc -l) == 1 \
    && echo \"[\$CHKTIME] Nuclos_AutoDbSetup_Check_1/2\"' ; do sleep 1 ; done" \
	&& nice -n 19 timeout 3600s bash -c "until bash -c 'test -f /opt/nuclos/home/logs/server.log \
    && PID=\$(jps -l | grep org.apache.catalina.startup.Bootstrap | cut -d \" \" -f 1) \
    && test ! -z \$PID && CHKTIME=\$(date \"+%Y-%m-%dT%H:%M:%S\") && JSTACK=\$(cpulimit --limit=50 --foreground --quiet -- jstack \$PID) \
    && test \$(echo \$JSTACK | grep \"AutoDbSetupComplete\" | wc -l) == 1 \
    && echo \"[\$CHKTIME] Nuclos_AutoDbSetup_Check_2/2\"' ; do sleep 1 ; done"
  while read LOG_LINE; do
    if [[ $LOG_LINE == *"Error auto db setup"* ]]; then
      # a good place for a future function: automatically restore to last data backup and downgrade with backup installer?
      sleep 5
      echo ">>> Error auto db setup <<<" >> $NUCLOS_HOME/logs/server.log
      BACKUP_INSTALLER=`find $NUCLOS_DIR/update -name nuclos-*-installer-generic.jar -print -quit`
      echo ">>> Try to restore a backup with:" >> $NUCLOS_HOME/logs/server.log
      echo "    nuclos-restore.sh $DB_SCHEMA#latest" >> $NUCLOS_HOME/logs/server.log
      echo ">>> And/or try to install the last Nuclos version with:" >> $NUCLOS_HOME/logs/server.log
      echo "    nuclos-install.sh $BACKUP_INSTALLER" >> $NUCLOS_HOME/logs/server.log
      return 1
    fi
    if [[ $LOG_LINE == *"AutoDbSetupComplete"* ]] || [[ $LOG_LINE == *"Auto-Init finished successfully"* ]] || [[ $LOG_LINE == *"Auto-Update finished successfully"* ]]; then
      echo "Nuclos AutoDbSetup completed"
      if [ "$CLUSTER_MODE" == "false" ] || ( [ "$CLUSTER_MODE" == "true" ] && [ "$CLUSTER_NODE_TYPE" == "master" ] ); then
        # Install or update Docker Nuclet
        nuclos-db-cmd.sh "$DOCKER_NUCLET" start-$DB_SCHEMA-docker-nuclet.sql
        # Do something after AutoDbSetup
        # ...
      fi
    fi
  done < $NUCLOS_HOME/logs/server.log

	# Wait for server startup
	nice -n 19 timeout 3600s bash -c "until bash -c 'test -f /opt/nuclos/home/logs/server.log \
    && PID=\$(jps -l | grep org.apache.catalina.startup.Bootstrap | cut -d \" \" -f 1) \
    && test ! -z \$PID && CHKTIME=\$(date \"+%Y-%m-%dT%H:%M:%S\") && JSTACK=\$(cpulimit --limit=5 --foreground --quiet -- jstack \$PID) \
    && ! test \$(echo \$JSTACK | grep \"NuclosQuartzScheduler_Worker\" | wc -l) == 0 \
    && test \$(echo \$JSTACK | grep \"AutoDbSetup\" | wc -l) == 0 \
    && echo \"[\$CHKTIME] Nuclos_Startup_Check_1/3\" \
    && test \$(echo \$JSTACK | grep \"Nuclet Extension Updater\" | wc -l) == 0 \
    && echo \"[\$CHKTIME] Nuclos_Startup_Check_2/3\" \
    && test \$(wget --timeout=10 --quiet -O- http://127.0.0.1:8080/nuclos/rest/serverstatus | grep \\\"ready\\\":true | wc -l) == 1 \
    && echo \"[\$CHKTIME] Nuclos_Startup_Check_3/3\"' ; do sleep 5 ; done"
  echo "Nuclos startup completed" >> $NUCLOS_HOME/logs/server.log
  TOMCAT_DIR=$(find $NUCLOS_HOME/tomcat -iname "apache-tomcat-*" -type d)
  if [ -d $NUCLOS_DIR/assets ]; then
    rsync -qplr --chmod=D777,F666 --ignore-missing-args $NUCLOS_DIR/assets/* $TOMCAT_DIR/webapps/ROOT/webclient/assets/
  fi
  if [ "$DEVELOPMENT" == "true" ]; then
    # Export codegen dependencies
    if [ -d $NUCLOS_HOME/data/codegenerator-dependencies ]; then
      rsync -qplr --del --chmod=D777,F666 --ignore-missing-args $NUCLOS_HOME/webapp/WEB-INF/* $NUCLOS_HOME/data/codegenerator-dependencies/
    fi
  fi

	sleep 10
	return 0
}

# tail server log outputs
nuclos-logs.sh &

# watch server log for restart instructions
nuclos-restart-watcher.sh &


startAppServer

# Keep nuclos alive and look for restarts, restores, updates etc.
rm -f $NUCLOS_DIR/keep-alive.disabled
while true; do
	if [ ${SCRIPT_SHUTDOWN} -eq 1 ]; then
		exit 0
	fi
	nice -n 19 timeout "${KEEP_ALIVE_CHECK_TIMEOUT}s" bash -c "PID=\$(jps -l | grep org.apache.catalina.startup.Bootstrap | cut -d \" \" -f 1) \
      && test ! -z \$PID && JSTACK=\$(cpulimit --limit=5 --foreground --quiet -- jstack \$PID) \
      && ! test \$(echo \$JSTACK | grep \"NuclosServerTimer\" | wc -l) == 0 \
      && echo \"Nuclos_KeepAlive_Check_1/1\"" > /dev/null 2>&1 \
	  && nc -z -w $KEEP_ALIVE_CHECK_TIMEOUT 127.0.0.1 $NUCLOS_PORT > /dev/null 2>&1
	if [ $? -ne 0 ]; then
		# Nuclos not running any more, restart
		echo "Keep-Alive-Check now performs a shutdown/restart"
		if [ ${SCRIPT_SHUTDOWN} -eq 0 ]; then
			nuclos-shutdown.sh
		fi
		if [ ${SCRIPT_SHUTDOWN} -eq 0 ]; then
			if [ ! -f $NUCLOS_DIR/keep-alive.disabled ]; then
				startAppServer
			fi
		fi
		sleep 30
	fi
	if [ -f $NUCLOS_DIR/nuclos.restart ]; then
		# Restart only
		echo "Restart Nuclos..."
		rm $NUCLOS_DIR/nuclos.restart
		# wait for the job to finish
		sleep 5
		nuclos-shutdown.sh
	fi
	if [ -f $NUCLOS_DIR/backup.restore ]; then
		# Restore a backup
		RESTORE_NAME=$(<$NUCLOS_DIR/backup.restore)
		rm $NUCLOS_DIR/backup.restore
		# wait for the job to finish
		sleep 5
		nuclos-restore.sh "$RESTORE_NAME"
	fi
	if [ -f $NUCLOS_DIR/nuclos.update ]; then
		# Update Nuclos
		NUCLOS_INSTALLER=$(<$NUCLOS_DIR/nuclos.update)
		rm $NUCLOS_DIR/nuclos.update
		# wait for the job to finish
		sleep 5
		nuclos-install.sh "$NUCLOS_INSTALLER"
		if [ -f $NUCLOS_DIR/update/job-session-id.txt ]; then
			JOB_SESSION_ID=$(<$NUCLOS_DIR/update/job-session-id.txt)
			JOB_LOG_SQL="set search_path to $DB_SCHEMA;"
			JOB_LOG_SQL+=$'\n'
			JOB_LOG_SQL+="INSERT INTO T_MD_JOBRUN_MESSAGES (INTID, INTID_T_MD_JOBRUN, STRMESSAGELEVEL, STRMESSAGE, STRRULE, DATCREATED, STRCREATED, DATCHANGED, STRCHANGED, INTVERSION) VALUES (NEXTVAL('IDFACTORY'), $JOB_SESSION_ID, 'INFO', 'Update complete', 'DockerCore', clock_timestamp(), USER, clock_timestamp(), USER, 1);"
			nuclos-db-cmd.sh "$JOB_LOG_SQL" "nuclos-update-complete-$DB_SCHEMA.sql"
			rm -f $NUCLOS_DIR/update/job-session-id.txt
		fi
	fi
	sleep 10
done
