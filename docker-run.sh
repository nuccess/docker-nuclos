#!/bin/bash
umask $UMASK

# terminate container function
function _container_SIGTERM(){
  echo "Caught SIGTERM - Nuclos: shutdown ($NUCLOS_HOME)"
  nuclos-shutdown.sh
  if [ "$BUILD_TYPE" = "with-db" ]; then
    echo "Caught SIGTERM - Database: stop ($PGDATA)"
    sudo -u postgres $PGCTL -D $PGDATA stop
  fi
  exit 0
}
trap _container_SIGTERM SIGTERM

# 1 = yes
export NO_NEW_PRIVILEGES=$(grep "NoNewPrivs" /proc/self/status | awk '{print $2}')

# Set the locale
if [ -n "$LOCALE" ]; then
	GREP_LOCALE=$(grep "^$LOCALE " /usr/share/i18n/SUPPORTED)
	if [ -n "$GREP_LOCALE" ]; then
		LOCALE_ENCODING=${GREP_LOCALE##* }
		LOCALE_WITHOUT_ENCODING=${LOCALE%.*}
		export LANG=$LOCALE_WITHOUT_ENCODING.$LOCALE_ENCODING
		echo "Set locale to $LANG"
		if [ "$NO_NEW_PRIVILEGES" -eq 1 ]; then
      export LC_MESSAGES=POSIX
		else
		  sudo localedef -i $LOCALE_WITHOUT_ENCODING -c -f $LOCALE_ENCODING /usr/lib/locale/$LANG
		  sudo update-locale LANG=$LANG LC_MESSAGES=POSIX
		fi
	else
		echo "Locale \"$LOCALE\" is not supported. See list of available /usr/share/i18n/SUPPORTED:"
		cat /usr/share/i18n/SUPPORTED
	fi
fi

export DB_PASSWORD=$(tr -d '\r\n' < /opt/nuclos/secrets/db_password || echo "password")
export KEYSTORE_PASSWORD=$(tr -d '\r\n' < /opt/nuclos/secrets/keystore_password || echo "password")

# try to make volumes (not contents) writeable
if [ "$NO_NEW_PRIVILEGES" -eq 1 ]; then
  chmod -f +rw $NUCLOS_HOME/data/documents
  chmod -f +rw $NUCLOS_HOME/data/index
  chmod -f +rw $NUCLOS_HOME/data/nucletimport
  chmod -f +rw $NUCLOS_HOME/data/codegenerator
  chmod -f +rw $NUCLOS_HOME/data/codegenerator-dependencies
  chmod -f +rw $NUCLOS_HOME/logs
  chmod -f +rw $NUCLOS_DIR/assets
  chmod -f +rw $NUCLOS_DIR/client
  chmod -f +rw $NUCLOS_DIR/extensions
  chmod -f +rw $NUCLOS_DIR/backups
else
  sudo -u $USERNAME chmod -f +rw $NUCLOS_HOME/data/documents
  sudo -u $USERNAME chmod -f +rw $NUCLOS_HOME/data/index
  sudo -u $USERNAME chmod -f +rw $NUCLOS_HOME/data/nucletimport
  sudo -u $USERNAME chmod -f +rw $NUCLOS_HOME/data/codegenerator
  sudo -u $USERNAME chmod -f +rw $NUCLOS_HOME/data/codegenerator-dependencies
  sudo -u $USERNAME chmod -f +rw $NUCLOS_HOME/logs
  sudo -u $USERNAME chmod -f +rw $NUCLOS_DIR/assets
  sudo -u $USERNAME chmod -f +rw $NUCLOS_DIR/client
  sudo -u $USERNAME chmod -f +rw $NUCLOS_DIR/extensions
  sudo -u $USERNAME chmod -f +rw $NUCLOS_DIR/backups
fi

if [ "$BUILD_TYPE" = "with-db" ]; then
  if [ "$NO_NEW_PRIVILEGES" -eq 1 ]; then
    echo "The environment does not allow postgres setup (--no-new-privileges)"
    exit 9;
  fi
  # Set postgres password
  export POSTGRES_PASSWORD=$DB_PASSWORD
  if  [ ! -d $PGDATA ]; then
    echo "Database: init ($PGDATA)"
  	sudo -u postgres nuclos-db-init.sh
  	echo
  fi
	echo "Database: configure ($PGDATA)"
	sudo -u postgres nuclos-db-config.sh
	# Start 'daemon' database command executor
	sudo -u postgres execute-db-cmds.sh &
	echo "Database: start ($PGDATA)"
	export PGCTL=$(find /usr/lib -name pg_ctl | grep /bin/)
	sudo -u postgres $PGCTL -D $PGDATA start >/dev/null 2>&1
	echo "Database: create $POSTGRES_DB and user $POSTGRES_USER if not already done"
  sudo -u postgres createuser -s $POSTGRES_USER >/dev/null 2>&1
  sudo -u postgres createdb -O $POSTGRES_USER $POSTGRES_DB >/dev/null 2>&1
  sudo -u postgres psql -c "ALTER USER $POSTGRES_USER WITH PASSWORD '$DB_PASSWORD';" >/dev/null
  echo "Database: set timezone to $TZ"
  sudo -u postgres psql -c "ALTER USER $POSTGRES_USER SET TIMEZONE='$TZ';" >/dev/null
fi

export POSTGRES_PORT=${POSTGRES_PORT_5432_TCP_PORT:-5432}
export POSTGRES_DB=${POSTGRES_ENV_POSTGRES_DB:-"nuclosdb"}
export POSTGRES_USER=${POSTGRES_ENV_POSTGRES_USER:-"nuclos"}

nuclos-run.sh &
wait
