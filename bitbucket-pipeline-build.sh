#!/bin/bash

set -e

export DOCKERHUB_REPO=nuclos-server
export BASE_IMAGE_TAG=$(grep "^ARG BASE_IMAGE_TAG" Dockerfile | cut -d '=' -f2)
echo "Current branch $BITBUCKET_BRANCH"
git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
git fetch --unshallow origin || true
LATEST_BRANCH=$(for branch in $(git for-each-ref --format='%(refname:short)' refs/remotes/origin/); \
  do echo "$(git log --reverse --format="%h" -1 $branch) ${branch#origin/}"; \
  done | sort -r | awk '{print $NF}' | grep -E '^[0-9]' | sort -Vr | head -n 1)
echo "Latest non-master branch: $LATEST_BRANCH"
if [ "$BITBUCKET_BRANCH" = "master" ]; then
  export DOCKER_TAG="$DOCKERHUB_REPO:latest"
  echo "$LATEST_BRANCH used for NUCLOS_BRANCH"
  export NUCLOS_BRANCH=$LATEST_BRANCH
else
  export DOCKER_TAG="$DOCKERHUB_REPO:$BITBUCKET_BRANCH"
  export NUCLOS_BRANCH=$BITBUCKET_BRANCH
fi
echo "Using Docker Tag $DOCKER_TAG"
echo "Step 1 Logging in to Docker Hub"
echo "$DOCKERHUB_PASSWORD" | docker login -u "$DOCKERHUB_USERNAME" --password-stdin
echo "Step 2 Decode and Place Keys"
mkdir -p ~/.docker/trust/private
echo "$NUCCESS_KEY_BASE64" | tr -d '\n\r\t ' | base64 --decode > ~/.docker/trust/private/$NUCCESS_KEY_FILE
echo "$SNAPSHOT_KEY_BASE64" | tr -d '\n\r\t ' | base64 --decode > ~/.docker/trust/private/$SNAPSHOT_KEY_FILE
chmod 600 ~/.docker/trust/private/*.key
export DOCKER_CONTENT_TRUST_REPOSITORY_PASSPHRASE=$DOCKER_CONTENT_TRUST_REPOSITORY_PASSPHRASE
docker trust key load ~/.docker/trust/private/$NUCCESS_KEY_FILE --name nuccess
docker trust key load ~/.docker/trust/private/$SNAPSHOT_KEY_FILE --name snapshot
docker trust inspect --pretty $DOCKERHUB_USERNAME/$DOCKERHUB_REPO
echo "Step 3 Build & Push Docker Image"
mkdir -p ~/.docker/cli-plugins/
curl -sL https://github.com/docker/buildx/releases/download/v0.17.1/buildx-v0.17.1.linux-amd64 -o ~/.docker/cli-plugins/docker-buildx
chmod +x ~/.docker/cli-plugins/docker-buildx
docker buildx version
docker buildx create --use
docker buildx build --tag $DOCKERHUB_USERNAME/$DOCKER_TAG \
  --build-arg NUCLOS_BRANCH=$NUCLOS_BRANCH \
  --platform linux/amd64 --push --sbom=true --provenance=true \
  --cache-from type=registry,ref=$DOCKERHUB_USERNAME/$DOCKERHUB_REPO:buildcache .
docker buildx build --tag $DOCKERHUB_USERNAME/$DOCKER_TAG-with-db \
  --build-arg WITH_DB_BASE_IMAGE_REPOSITORY=$DOCKERHUB_USERNAME/ \
  --build-arg WITH_DB_BASE_IMAGE_DOCKER_TAG=$DOCKER_TAG \
  --platform linux/amd64 --push --sbom=true --provenance=true \
  --cache-from type=registry,ref=$DOCKERHUB_USERNAME/$DOCKERHUB_REPO:buildcache with-db
echo "Step 4 Sign Docker Image"
docker pull $DOCKERHUB_USERNAME/$DOCKER_TAG
docker pull $DOCKERHUB_USERNAME/$DOCKER_TAG-with-db
export DOCKER_CONTENT_TRUST=1
docker trust sign $DOCKERHUB_USERNAME/$DOCKER_TAG
docker trust sign $DOCKERHUB_USERNAME/$DOCKER_TAG-with-db
echo "Step 5 Push sbom and provenance after sign"
docker buildx build --tag $DOCKERHUB_USERNAME/$DOCKER_TAG \
  --build-arg NUCLOS_BRANCH=$NUCLOS_BRANCH \
  --platform linux/amd64 --push --sbom=true --provenance=true \
  --cache-from type=registry,ref=$DOCKERHUB_USERNAME/$DOCKERHUB_REPO:buildcache \
  --cache-to type=registry,ref=$DOCKERHUB_USERNAME/$DOCKERHUB_REPO:buildcache,mode=max .
docker buildx build --tag $DOCKERHUB_USERNAME/$DOCKER_TAG-with-db \
  --build-arg WITH_DB_BASE_IMAGE_REPOSITORY=$DOCKERHUB_USERNAME/ \
  --build-arg WITH_DB_BASE_IMAGE_DOCKER_TAG=$DOCKER_TAG \
  --platform linux/amd64 --push --sbom=true --provenance=true \
  --cache-from type=registry,ref=$DOCKERHUB_USERNAME/$DOCKERHUB_REPO:buildcache \
  --cache-to type=registry,ref=$DOCKERHUB_USERNAME/$DOCKERHUB_REPO:buildcache,mode=max with-db
if [ "$BITBUCKET_BRANCH" = "$LATEST_BRANCH" ]; then
  echo "Step 6 Push additional as latest"
  docker image tag $DOCKERHUB_USERNAME/$DOCKER_TAG $DOCKERHUB_USERNAME/$DOCKERHUB_REPO:latest
  docker image tag $DOCKERHUB_USERNAME/$DOCKER_TAG-with-db $DOCKERHUB_USERNAME/$DOCKERHUB_REPO:latest-with-db
  docker trust sign $DOCKERHUB_USERNAME/$DOCKERHUB_REPO:latest
  docker trust sign $DOCKERHUB_USERNAME/$DOCKERHUB_REPO:latest-with-db
  docker buildx build --tag $DOCKERHUB_USERNAME/$DOCKERHUB_REPO:latest \
    --build-arg NUCLOS_BRANCH=$NUCLOS_BRANCH \
    --platform linux/amd64 --push --sbom=true --provenance=true .
  docker buildx build --tag $DOCKERHUB_USERNAME/$DOCKERHUB_REPO:latest-with-db \
    --build-arg WITH_DB_BASE_IMAGE_REPOSITORY=$DOCKERHUB_USERNAME/ \
    --build-arg WITH_DB_BASE_IMAGE_DOCKER_TAG=$DOCKER_TAG \
    --platform linux/amd64 --push --sbom=true --provenance=true with-db
fi
if [ "$BITBUCKET_BRANCH" = "master" ] || [ "$BITBUCKET_BRANCH" = "$LATEST_BRANCH" ]; then
  echo "Step 7 Update Docker Hub Description"
  export DB_IMAGE_TAG=17.1
  sed -i "s/BASE_IMAGE_TAG_PLACEHOLDER/$BASE_IMAGE_TAG/g" README.md
  sed -i "s/DB_IMAGE_TAG_PLACEHOLDER/$DB_IMAGE_TAG/g" README.md
  sed -i "s/NUCLOS_BRANCH_PLACEHOLDER/$NUCLOS_BRANCH/g" README.md
  sed -i "s/DOCKERHUB_REPO_PLACEHOLDER/$DOCKERHUB_REPO/g" README.md
  description='{"full_description": '"$(cat README.md | jq -Rs '.')"'}'
    curl -sX PATCH https://hub.docker.com/v2/repositories/$DOCKERHUB_USERNAME/$DOCKERHUB_REPO/ \
      -H "Authorization: Bearer $DOCKERHUB_PASSWORD" \
      -H "Content-Type: application/json" \
      -d "$description"
    echo "Description successfully updated"
fi
