#!/bin/bash

while [ $(ps aex | grep tomcat | grep java | wc -l) -gt 0 ] ; do
  sleep 0.2
done
echo "Tomcat process terminated"
