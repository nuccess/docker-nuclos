#
# Nuclos Dockerfile 
#
# https://bitbucket.org/nuccess/docker-nuclos
#
ARG BASE_IMAGE_TAG=17-noble
FROM eclipse-temurin:8-noble AS build
RUN mkdir -p /root/.m2 && \
    mkdir -p /root/.npm && \
    mkdir -p /root/.3rdparty
RUN --mount=type=cache,target=/root/.m2 \
    --mount=type=cache,target=/root/.npm \
    --mount=type=cache,target=/root/.3rdparty

ARG NUCLOS_BRANCH=missing
ARG DOWNLOAD_URL=compile
ARG DOWNLOAD_HASH=off

WORKDIR /build

RUN apt-get update
#RUN apt-get upgrade -y
RUN apt-get install -y git maven curl
RUN git clone --depth 2 -b "$NUCLOS_BRANCH" --single-branch https://bitbucket.org/nuclos/nuclos.git nuclos-git

WORKDIR /build/nuclos-git

RUN cat pom.xml | grep -oPm1 "(?<=<node.version>)[^<]+" > node.version
RUN cat pom.xml | grep -oPm1 "(?<=<npm.version>)[^<]+" > npm.version

# Patch Nuccess CI
RUN rm -fv nuclos-client/src/main/resources/org/nuclos/client/images/Preloader5.*.png
RUN rm -fv nuclos-client/src/main/resources/org/nuclos/client/images/splashScreen.gif
ADD Nuccess.ico                   nuclos-client/src/main/resources/icons/nuccess.ico
ADD Nuccess-ico-128.png           nuclos-client/src/main/resources/icons/nuclos-icon.png
ADD Nuccess-ico-512.png           nuclos-client/src/main/resources/icons/nuclos-icon_512.png
ADD Nuccess-Animation-1-1-214.png nuclos-client/src/main/resources/org/nuclos/client/images/Preloader5.50001.png
ADD Nuccess-Animation-1-2-214.png nuclos-client/src/main/resources/org/nuclos/client/images/Preloader5.50002.png
ADD Nuccess-Animation-1-3-214.png nuclos-client/src/main/resources/org/nuclos/client/images/Preloader5.50003.png
ADD Nuccess-Animation-1-4-214.png nuclos-client/src/main/resources/org/nuclos/client/images/Preloader5.50004.png
ADD Nuccess-Animation-1-5-214.png nuclos-client/src/main/resources/org/nuclos/client/images/Preloader5.50005.png
ADD Nuccess-Animation-1-6-214.png nuclos-client/src/main/resources/org/nuclos/client/images/Preloader5.50006.png
ADD Nuccess-Animation-1-7-214.png nuclos-client/src/main/resources/org/nuclos/client/images/Preloader5.50007.png
ADD Nuccess-rootpane-bg.png       nuclos-client/src/main/resources/org/nuclos/client/main/mainframe/background.png
ADD favicon.ico                   nuclos-war/src/main/webapp/favicon.ico
ADD favicon.png                   nuclos-webclient/src/assets/favicon.png
ADD Nuccess-navbar-bg.png         nuclos-webclient/src/assets/navbar-bg.png
ADD Nuccess-128.png               nuclos-webclient/src/assets/nuclos-logo.png
ADD Nuccess-Animation-2-128.gif   nuclos-webclient/src/assets/nuclos-rotating-48.gif
ADD Nuccess-44.png                nuclos-webclient/src/assets/nuclos-signet.png
ADD nuccess-ci.patch .
RUN git apply nuccess-ci.patch
# Only the generic installer is required:
ADD build.xml                     nuclos-installer
RUN sed -i '/<execution>/ { :a; N; /<\/execution>/!ba; /<id>build.*win.*<\/id>/d }' nuclos-installer/pom.xml
RUN sed -i '/<module>nuclos-integration-tests.*<\/module>/d' pom.xml

ENV MAVEN_OPTS="-Xmx2048m" \
  NODE_OPTIONS="--max-old-space-size=2048"

RUN if [ "$DOWNLOAD_URL" = "compile" ] ; then mvn clean package -B -ntp \
   	-Ddependency.check.skip=true \
   	-Dmaven.test.skip=true \
   	-Dmaven.javadoc.skip=true \
   	&& mv nuclos-installer/dist / ; fi
RUN if [ ! "$DOWNLOAD_URL" = "compile" ] ; then wget --tries=3 --wait=60 --timeout=30 --no-dns-cache $DOWNLOAD_URL && mkdir /dist && mv *.jar /dist ; fi
RUN if [ ! "$DOWNLOAD_HASH" = "off" ] ; then export DOWNLOAD_FILE="${DOWNLOAD_URL##*/}" && test `md5sum /dist/$DOWNLOAD_FILE | awk '{ print $1 }'` = $DOWNLOAD_HASH ; fi


FROM eclipse-temurin:${BASE_IMAGE_TAG}
RUN mkdir -p /var/cache/apt && \
    mkdir -p /var/lib/apt/lists
RUN --mount=type=cache,target=/var/cache/apt \
    --mount=type=cache,target=/var/lib/apt/lists

RUN apt-get update && apt-get upgrade -y && apt-get install -y curl

# Install node and npm
COPY --from=build /build/nuclos-git/node.version /tmp
COPY --from=build /build/nuclos-git/npm.version /tmp
ENV NVM_DIR=/opt/nvm
RUN mkdir -p $NVM_DIR && \
  curl https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash && \
  . $NVM_DIR/nvm.sh && \
  export NODE_VERSION=$(cat /tmp/node.version | tr -d '\n') && \
  export NPM_VERSION=$(cat /tmp/npm.version | tr -d '\n') && \
  nvm install ${NODE_VERSION#v} && \
  npm install -g npm@${NPM_VERSION#v} && \
  echo "Add symlinks for the node + npm commands that should be available for Nuclos" && \
  ln -sf $NVM_DIR/versions/node/v${NODE_VERSION#v}/bin/node /usr/bin/node && \
  ln -sf $NVM_DIR/versions/node/v${NODE_VERSION#v}/bin/npm /usr/bin/npm && \
  echo $(node -v) && test "$(node -v)" = "v${NODE_VERSION#v}" && \
  echo $(npm -v) && test "$(npm -v)" = "${NPM_VERSION#v}"

RUN apt-get update && apt-get upgrade -y && \
  apt-get install -y sudo rsync netcat-openbsd locales vim zip unzip cpulimit

ENV BUILD_TYPE=default \
  NUCLOS_DIR=/opt/nuclos \
  USERNAME=nuclos \
  GROUPNAME=nuclos
RUN \
  mkdir -p $NUCLOS_DIR && \
  groupmod -g 11 ubuntu && \
  usermod -u 11 ubuntu && \
  groupadd -g 1000 $GROUPNAME && \
  useradd -u 1000 -g 1000 -md /home/$USERNAME $USERNAME && \
  usermod -aG sudo $USERNAME && \
  echo "$USERNAME ALL=(ALL) NOPASSWD: /usr/bin/localedef" >> /etc/sudoers && \
  echo "$USERNAME ALL=(ALL) NOPASSWD: /usr/sbin/update-locale" >> /etc/sudoers && \
  echo "$USERNAME ALL=(ALL) NOPASSWD: /usr/bin/chmod" >> /etc/sudoers && \
  echo "$USERNAME ALL=(ALL) NOPASSWD: /usr/bin/chown" >> /etc/sudoers && \
  chown -R $USERNAME:$GROUPNAME $NUCLOS_DIR
USER 1000:1000

# Confirm environment twice
RUN export NODE_VERSION=$(cat /tmp/node.version | tr -d '\n') && \
  export NPM_VERSION=$(cat /tmp/npm.version | tr -d '\n') && \
  echo $(node -v) && test "$(node -v)" = "v${NODE_VERSION#v}" && \
  echo $(npm -v) && test "$(npm -v)" = "${NPM_VERSION#v}"

ENV NUCLOS_HOME=$NUCLOS_DIR/home
ENV NUCLOS_CONF=$NUCLOS_HOME/conf \
  NUCLOS_STARTUP=$NUCLOS_HOME/bin/startup.sh
ENV NUCLOS_DIR=/opt/nuclos \
  NUCLOS_PORT=8080 \
  NUCLOS_DEBUG_PORT=8000 \
  NUCLOS_MAX_RAM_PERCENTAGE=75 \
  NUCLOS_CONNECTION_THREADPOOL_SIZE=200 \
  NUCLOS_WEBCLIENT_CONF=webapps/ROOT/webclient/assets/config.json \
  NUCLOS_TOMCAT_CONF=conf/server.xml \
  CLIENT_CONFIG_SERVER_HOST=localhost \
  CLIENT_CONFIG_MAX_HEAP=1024m \
  DB_SCHEMA=nuclos \
  DB_CONNECTIONPOOL_SIZE=50 \
  DB_CONNECTIONPOOL_WAITSECONDS=120 \
  DOCKER_NUCLOS_PORT=8080 \
  CLUSTER_MODE=false \
  CLUSTER_NODE_TYPE=master \
  CLUSTER_NODE_ID=nuccess \
  CLUSTER_NODE_HOSTNAME=master.example.nuc \
  CLUSTER_BALANCER_PROTOCOL=http \
  CLUSTER_BALANCER_HOSTNAME=balancer.example.nuc \
  CLUSTER_BALANCER_PORT=80 \
  CLUSTER_BALANCER_CONTEXT=nuclos \
  LIVE_SEARCH=false \
  DEVELOPMENT=false \
  LOCALE=de_DE.UTF-8 \
  TZ=Europe/Berlin \
  TOTAL_RAM_GB=2.0 \
  NUCLOS_MAX_RAM_PERCENTAGE=75 \
  DB_MAX_RAM_PERCENTAGE=10 \
  KEEP_ALIVE_CHECK_TIMEOUT=300

WORKDIR $NUCLOS_DIR

ADD nuclos-template.xml .
ADD docker-nuc-bridge-4.2024.18.0001.jar .
ADD docker-nuc-utils-4.2024.18.0001.jar .
ADD docker-nuc-rules-4.2024.18.0001.jar .
ADD docker-4.2024.18.0001_01_application.sql .
ADD docker-4.2024.18.0001_02_nucletparameter.sql .
ADD docker-4.2024.18.0001_03_jobqueue.sql .
ADD docker-4.2024.18.0001_04_jobcontroller.sql .
ADD docker-4.2024.18.0001_05_servercode_job.sql .

USER root

ADD docker-run.sh /usr/local/bin
ADD nuclos-run.sh /usr/local/bin
ADD nuclos-db-cmd.sh /usr/local/bin
ADD nuclos-logs.sh /usr/local/bin
ADD nuclos-config.sh /usr/local/bin
ADD nuclos-install.sh /usr/local/bin
ADD nuclos-extensions.sh /usr/local/bin
ADD nuclos-shutdown.sh /usr/local/bin
ADD nuclos-restore.sh /usr/local/bin
ADD nuclos-restart-watcher.sh /usr/local/bin
ADD tomcat-wait-for-termination.sh /usr/local/bin

RUN \
  chmod +x /usr/local/bin/*.sh

# Make sure that new files can be edited on mounted volumes
ENV UMASK=0
RUN \
  umask $UMASK && \
  echo "umask 0" >> /root/.bashrc && \
  echo "umask 0" >> /root/.profile && \
  echo "umask 0" >> /home/$USERNAME/.bashrc && \
  echo "umask 0" >> /home/$USERNAME/.profile 

RUN \
  chown $USERNAME:$GROUPNAME * && \
  chown $USERNAME:$GROUPNAME /root/.* && \
  chown -R $USERNAME:$GROUPNAME /home/$USERNAME/.* && \
  chown -R $USERNAME:$GROUPNAME $NUCLOS_DIR

COPY --from=build --chown=$USERNAME:$GROUPNAME /dist/*.jar $NUCLOS_DIR/update/backup/
RUN if ls $NUCLOS_DIR/update/backup/*.jar 1> /dev/null 2>&1; then \
    echo "Found Nuclos installer:"; \
    ls -lh $NUCLOS_DIR/update/backup/*.jar; \
  else \
    echo "No Nuclos installer found!"; \
    exit 1; \
  fi

USER 1000:1000

WORKDIR $NUCLOS_HOME

RUN \
  cp $NUCLOS_DIR/nuclos-template.xml nuclos.xml && \
  sed -i 's:NUCLOS_HOME:'"$NUCLOS_HOME"':g' nuclos.xml && \
  sed -i 's:NUCLOS_DOCUMENTS:'"$NUCLOS_HOME/data/documents"':g' nuclos.xml && \
  sed -i 's:NUCLOS_PORT:'"$NUCLOS_PORT"':g' nuclos.xml && \
  sed -i 's:JAVA_HOME:'"$JAVA_HOME"':g' nuclos.xml

# Disable asking for Angular analytics
ENV NG_CLI_ANALYTICS=ci

# Create volume directories here, otherwise they may be owned by root and cannot be used
RUN \
  mkdir -p -m 555 $NUCLOS_HOME/data/documents && \
  mkdir -p -m 555 $NUCLOS_HOME/data/index && \
  mkdir -p -m 555 $NUCLOS_HOME/data/nucletimport && \
  mkdir -p -m 555 $NUCLOS_HOME/data/codegenerator && \
  mkdir -p -m 555 $NUCLOS_HOME/data/codegenerator-dependencies && \
  mkdir -p -m 555 $NUCLOS_HOME/logs && \
  mkdir -p -m 555 $NUCLOS_DIR/assets && \
  mkdir -p -m 555 $NUCLOS_DIR/extensions && \
  mkdir -p -m 555 $NUCLOS_DIR/backups

# Add VOLUMEs to access documents, logs and backups
VOLUME ["$NUCLOS_HOME/data/documents", "$NUCLOS_HOME/data/index", "$NUCLOS_HOME/data/nucletimport", "$NUCLOS_HOME/data/codegenerator", "$NUCLOS_HOME/data/codegenerator-dependencies", "$NUCLOS_HOME/logs", "$NUCLOS_DIR/assets", "$NUCLOS_DIR/extensions", "$NUCLOS_DIR/backups", "$NUCLOS_DIR/secrets", "/var/nuclos-db"]

# Expose the nuclos ports
EXPOSE 8080 8000 8009 8033 5432

# Started with the specified user
USER $USERNAME

STOPSIGNAL SIGTERM
# Set the default command to run when starting the container
CMD ["/usr/local/bin/docker-run.sh"]
