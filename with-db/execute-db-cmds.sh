#!/bin/bash

# wait for commands (inotify does not work with mounted docker volumes)
#inotifywait -m $EXECUTE_CMD_DIR -e create -e moved_to |
#while read path action FILE; do

SCRIPT_SHUTDOWN=0

# Install shutdown trap
function scriptshutdown {
	SCRIPT_SHUTDOWN=1
}
trap scriptshutdown EXIT

# look every 5 seconds for files 
while true; do
	sleep 5
	if [ ${SCRIPT_SHUTDOWN} -eq 1 ]; then
		exit 0
	fi
	
	# Import cmds
	for FILE in `find $EXECUTE_CMD_DIR -name "*.imp" -type f`; do
		FILENAME="${FILE##*/}"
		TIMESTAMP=`date "+%Y%m%d-%H%M%S"`
		LOGFILE=$EXECUTE_CMD_LOGS_DIR/$TIMESTAMP-$FILENAME.log
		echo "$TIMESTAMP Import: $FILENAME" >> $LOGFILE
		export PGPASSWORD=$DB_PASSWORD
		FILENAME="${FILENAME%.*}"  # without extension
		PGSCHEMA=${FILENAME%\#*}  # retain the part before the number sign
		psql --command="DROP SCHEMA IF EXISTS $PGSCHEMA CASCADE;" --dbname=postgresql://$POSTGRES_USER:$DB_PASSWORD@localhost/$POSTGRES_DB >> $LOGFILE
		psql --command="CREATE SCHEMA $PGSCHEMA AUTHORIZATION $POSTGRES_USER;" --dbname=postgresql://$POSTGRES_USER:$DB_PASSWORD@localhost/$POSTGRES_DB >> $LOGFILE
		DUMPFILE="$EXECUTE_CMD_BACKUPS_DIR/$FILENAME.backup"
		pg_restore --host localhost --port 5432 --username "$POSTGRES_USER" --no-password --schema "$PGSCHEMA" --dbname "$POSTGRES_DB" --no-tablespaces --no-owner "$DUMPFILE" >> $LOGFILE
		rm $FILE
		TIMESTAMP=`date "+%Y%m%d-%H%M%S"`
		echo "$TIMESTAMP Finish import: $DUMPFILE" >> $LOGFILE
	done
	
	# SQL cmds
	for FILE in `find $EXECUTE_CMD_DIR -name "*.sql" -type f`; do
		FILENAME="${FILE##*/}"
		TIMESTAMP=`date "+%Y%m%d-%H%M%S"`
		LOGFILE=$EXECUTE_CMD_LOGS_DIR/$TIMESTAMP-$FILENAME.log
		echo "$TIMESTAMP Execute: $FILENAME" >> $LOGFILE
		cat $FILE >> $LOGFILE
		psql --file="$FILE" --dbname=postgresql://$POSTGRES_USER:$DB_PASSWORD@localhost/$POSTGRES_DB >> $LOGFILE
		rm $FILE
		TIMESTAMP=`date "+%Y%m%d-%H%M%S"`
		echo "$TIMESTAMP Finish execute: $FILE" >> $LOGFILE
	done
	
	# Export cmds
	for FILE in `find $EXECUTE_CMD_DIR -name "*.exp" -type f`; do
		FILENAME="${FILE##*/}"
		TIMESTAMP=`date "+%Y%m%d-%H%M%S"`
		LOGFILE=$EXECUTE_CMD_LOGS_DIR/$TIMESTAMP-$FILENAME.log
		echo "$TIMESTAMP Export: $FILENAME" >> $LOGFILE
		export PGPASSWORD=$DB_PASSWORD
		FILENAME="${FILENAME%.*}"  # without extension
		PGSCHEMA=${FILENAME%\#*}  # retain the part before the number sign
		DUMPFILE="$EXECUTE_CMD_BACKUPS_DIR/$FILENAME.backup"
		pg_dump --host localhost --port 5432 --username "$POSTGRES_USER" --format custom --encoding UTF8 --file "$DUMPFILE" --schema "$PGSCHEMA" "$POSTGRES_DB" --no-password >> $LOGFILE
		rm $FILE
		TIMESTAMP=`date "+%Y%m%d-%H%M%S"`
		echo "$TIMESTAMP Finish export: $DUMPFILE" >> $LOGFILE
	done
done
