#!/bin/bash
export PGINIT=$(find /usr/lib -name initdb | grep /bin/)
$PGINIT --auth-host=scram-sha-256 --locale-provider=icu --icu-locale=$LOCALE $PGDATA
# allow access from outside the container
echo "host    all             all             0.0.0.0/0               scram-sha-256" >> $PGDATA/pg_hba.conf
