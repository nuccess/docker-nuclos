# Nuclos

This repository contains [Nuclos](https://www.nuclos.de/) and is build by an automated pipeline and published to the public [Docker Hub Registry](https://registry.hub.docker.com/).

> ⚠️ **Important change**: Starting with version 4.2024.25 
> * All Docker images will be signed using [Docker Content Trust (DCT)](https://docs.docker.com/engine/security/trust/). 
> * Java Webstart is no longer supported. 
> * The `DB_PASSWORD` and `KEYSTORE_PASSWORD` parameter are no longer available. For security reasons, they are replaced by the files `db_password` and `keystore_password` in the volume `/opt/nuclos/secrets` . However, the default password has not changed, and an adaption will only be necessary if you have changed it.

## Base Docker Image

* [eclipse-temurin:BASE_IMAGE_TAG_PLACEHOLDER](https://hub.docker.com/_/eclipse-temurin/)

## Docker Tags

Visit [Tags](https://hub.docker.com/r/nuccess/DOCKERHUB_REPO_PLACEHOLDER/tags/)

## Source Code

Visit [BitBucket](https://bitbucket.org/nuccess/docker-nuclos/src/master/)

## Installation

1. Install [Docker](https://www.docker.com/).

2. Download automated build from public [Docker Hub Registry](https://registry.hub.docker.com/): `docker pull nuccess/nuclos`

   (alternatively, you can build an image from Dockerfile: `docker build --build-arg NUCLOS_BRANCH=NUCLOS_BRANCH_PLACEHOLDER --tag my-nuclos https://bitbucket.org/nuccess/docker-nuclos.git`)


---

# Usage

The Nuclos containers can either be operated with a separately started PostgreSQL database ([Nuclos-DB](https://hub.docker.com/r/nuccess/nuclos-db/)) or you can use the embedded database variant of the base image (tag names end with "-with-db"). In the second case, the version of the database is specified by the base image and the default port is `5432` with a default PostgreSQL account `nuclos` and password `password`.

You can affect the behavior of the container with a variety of environment variables and volumes. The most important are described below. **A running Nuclos DB container named nuclos-db is required for the most examples given here.** Local directory examples */Users/Maik/Nuclos/Docker/...* must be replaced.

## Development Environment  
###### for the latest Nuclos version
This is a good start for a default development environment with the embedded PostgreSQL database.

```shell
docker run --rm -d --name nuclos-dev -p 8060:8080 -p 5432:5432 -e DOCKER_NUCLOS_PORT=8060 \
-e DEVELOPMENT=true -e TZ=Europe/Berlin -e LOCALE=de_DE.UTF-8 \
nuccess/DOCKERHUB_REPO_PLACEHOLDER:latest-with-db
```

If more flexibility is required and the database is to be run separately, there is an alternative:

```shell
docker run --rm -d --name nuclos-dev -p 8060:8080 -e DOCKER_NUCLOS_PORT=8060 \
-e DB_SCHEMA=dev -e DEVELOPMENT=true -e TZ=Europe/Berlin -e LOCALE=de_DE.UTF-8 \
--volumes-from nuclos-db --link nuclos-db:POSTGRES \
nuccess/DOCKERHUB_REPO_PLACEHOLDER
```

Here you will find advanced options for easy access to logs `/opt/nuclos/home/logs`, generated Java source files `/opt/nuclos/home/data/codegenerator` and a Java debug port `8000`:

```shell
docker run --rm -d --name nuclos-dev -p 8060:8080 -p 18060:8000 -e DOCKER_NUCLOS_PORT=8060 \
-e DB_SCHEMA=dev \
-v /Users/Maik/Nuclos/Docker/vol-nuclos-codegen:/opt/nuclos/home/data/codegenerator \
-v /Users/Maik/Nuclos/Docker/vol-nuclos-devlogs:/opt/nuclos/home/logs \
-e DEVELOPMENT=true -e TZ=Europe/Berlin -e LOCALE=de_DE.UTF-8 \
--volumes-from nuclos-db --link nuclos-db:POSTGRES \
nuccess/DOCKERHUB_REPO_PLACEHOLDER
```

## Production Environment  
###### for Nuclos vNUCLOS_BRANCH_PLACEHOLDER
For a productive environment it is recommended to place the document attachments outside the container. 
An update of the Nuclos version therefore only consists of replacing the Nuclos container without having to migrate the document attachments in a manual step. 
For a secure SSL connection the keystore must be set using volume `/opt/nuclos/keystore` and password file `/opt/nuclos/secrets/keystore_password`.

```shell
docker run -d --name nuclos-prod -p 8040:8080 -e DOCKER_NUCLOS_PORT=8040 \
-e DB_SCHEMA=production \
-v /Users/Maik/Nuclos/Docker/vol-nuclos-secrets:/opt/nuclos/secrets:ro \
-v /Users/Maik/Nuclos/Docker/vol-nuclos-keystore:/opt/nuclos/keystore:ro \
-v /Users/Maik/Nuclos/Docker/vol-nuclos-backups:/opt/nuclos/backups \
-v /Users/Maik/Nuclos/Docker/vol-nuclos-documents:/opt/nuclos/home/data/documents \
-e TOTAL_RAM_GB=4 -e TZ=Europe/Berlin -e LOCALE=de_DE.UTF-8 \
--volumes-from nuclos-db --link nuclos-db:POSTGRES \
nuccess/DOCKERHUB_REPO_PLACEHOLDER:NUCLOS_BRANCH_PLACEHOLDER
```

⚠️ **Do not run productive, test and development environments in the same Nuclos-DB instance.**
The restore process of a DockerBackup first deletes the contained schema if it exists. So, if you want to import a DockerBackup of production into a test environment, and these two run in the same Nuclos-DB instance, the production will be deleted!


## Test Environment  
###### for Nuclos vNUCLOS_BRANCH_PLACEHOLDER
Here is the example of a test environment based on a restore of a productive environment. The special feature is the volume */opt/nuclos/restorescripts*

```shell
docker run --rm -d --name nuclos-test -p 8050:8080 -e DOCKER_NUCLOS_PORT=8050 \
-e DB_SCHEMA=test \
-v /Users/Maik/Nuclos/Docker/vol-nuclos-backups:/opt/nuclos/backups \
-v /Users/Maik/Nuclos/Docker/vol-nuclos-restore:/opt/nuclos/restorescripts:ro \
-e DEVELOPMENT=true -e TZ=Europe/Berlin -e LOCALE=de_DE.UTF-8 \
--volumes-from nuclos-db --link nuclos-db:POSTGRES \
nuccess/DOCKERHUB_REPO_PLACEHOLDER:NUCLOS_BRANCH_PLACEHOLDER
```

The Nuclos **DockerRestore** job restores a DockerBackup taken from production. After a restore the \*.sql scripts in `/opt/nuclos/restorescripts` are executed automatically. Please also read the warning in the production environment section.
Useful examples after a restoration could be:

* Change Nuclos system paramter value of NUCLOS_INSTANCE_NAME
```sql
UPDATE T_AD_PARAMETER SET STRVALUE='TEST Instance', STRCHANGED='restorescript',  
DATCHANGED=clock_timestamp() WHERE STRPARAMETER='NUCLOS_INSTANCE_NAME';
```

* Change a Nuclet parameter to disable productive features, such as sending mails to customers
```sql
UPDATE T_MD_NUCLETPARAMETER SET STRVALUE='n', STRCHANGED='restorescript', DATCHANGED=clock_timestamp()  
WHERE STRPARAMETER='Produktivsystem';
```

* Stop all Nuclos jobs
```sql
DELETE FROM T_AD_QRTZ_FIRED_TRIGGERS;  
DELETE FROM T_AD_QRTZ_SIMPLE_TRIGGERS;  
DELETE FROM T_AD_QRTZ_CRON_TRIGGERS;  
DELETE FROM T_AD_QRTZ_TRIGGERS;  
UPDATE T_MD_JOBCONTROLLER SET STRJOBLASTSTATE='Deaktiviert', BLNRUNNING=false,  
STRCHANGED='restorescript', DATCHANGED=clock_timestamp();
```

* Remove user passwords
```sql
UPDATE T_MD_USER SET STRPASSWORD=null, STRCHANGED='restorescript', DATCHANGED=clock_timestamp();
```

* Disable LDAP
```sql
UPDATE T_AD_LDAPSERVER SET BLNACTIVE=false;
```

---

## Nuclos Clients

* ### Nuclos Webclient  
The Webclient runs on the specified port: `-p 8060:8080 -e DOCKER_NUCLOS_PORT=8060`  
[http://localhost:8060](http://localhost:8060)

* ### Nuclos Desktop Client (via Nuclos Launcher [Download...](https://www.nuclos.de/downloads/nuclos-launcher/)) 
On the start page you will find a link to start via the Nuclos Launcher.  
Alternatively, you can add it manually. The server address for our example is: `nuclos://localhost:8060/nuclos`

* ### Nuclos Landing Page  
To visit the Nuclos default start page in your browser simply add `/nuclos` to the webclient URL  
[http://localhost:8060/nuclos](http://localhost:8060/nuclos)

* ### Default User  
The default user is `nuclos` with an empty password.

---

## Data Volumes

* ### `/opt/nuclos/secrets`
Directory of all secrets if you want to use something other than the default.
#### db_password
Contains the password to be set for the `POSTGRES_USER`
#### keystore_password
Contains the password for the specified keystore `/opt/nuclos/keystore`.

* ### `/opt/nuclos/backups`  
Directory for all backups made by the DockerBackup Job. For more information visit the [Nuclos wiki](https://bitbucket.org/nuccess/docker-nuclos/wiki/Home/)

* ### `/opt/nuclos/home/data/codegenerator`  
Contains generated Java source files. When development is enabled `-e DEVELOPMENT=true`, changes to rule sources (Directory *src-rule*) are automatically detected and compiled by Nuclos. Since it does not work everywhere, depending on the docker environment, a manual import can be initiated via the Desktop Client (Configuration/Rules/Rules (server) -> Toolbar *Import file changes*)

* ### `/opt/nuclos/home/data/codegenerator-dependencies`  
Contains Codegenerator dependencies. When development is enabled `-e DEVELOPMENT=true`, the dependencies are synchronized in this directory at startup. Then configure the Nuclos system parameters `CODEGENERATOR_POM_(\*_)LIBDIR` so that the paths of the automatically generated pom.xml match your local system and your IDE is able to find all dependencies. 
For example `CODEGENERATOR_POM_LIBDIR=${pom.basedir}/../codegen-deps/lib/` (Should actually always fit if the two directories are in the same main directory).

* ### `/opt/nuclos/home/data/documents`  
Storage of Nuclos managed document attachments.

* ### `/opt/nuclos/home/data/index`  
Storage of Nuclos live search index files.

* ### `/opt/nuclos/home/data/nucletimport`  
Nuclet dir for auto imports during startup.

* ### `/opt/nuclos/home/logs`  
Nuclos server log files.

* ### `/opt/nuclos/extensions`  
Nuclos server and client extensions. Same use as with a Nuclos installer.

* ### `/opt/nuclos/assets`  
Add or overwrite Nuclos webclient assets such as the favicon. Simply place a file called `favicon.png` in this volume, and the container will do the rest.

* ### `/opt/nuclos/keystore`  
Keystore file for a secure SSL connection between client and server. For more information see *Production Environment*.

* ### `/opt/nuclos/restorescripts`  
After a restore these scripts are executed automatically. For more information see *Test Environment*.

---

## Environment Variables

* ### DOCKER_NUCLOS_PORT
Please use the same port as in the mapping *-p 8060:8080*. Default is 8080 `-e DOCKER_NUCLOS_PORT=8060`

* ### TOTAL_RAM_GB
Set total usable RAM for the Container. If the container is started with a limit (--memory), this has priority. Default is 2.0 GB. `-e TOTAL_RAM_GB=2.0`

* ### NUCLOS_MAX_RAM_PERCENTAGE
The actual memory usage of a Java process under load exceeds the value for a maximum heap (Xmx). This parameter dynamically controls the heap depending on the available TOTAL_RAM, similar to the Java parameter XX:MaxRAMPercentage. Default is 75%.  `-e NUCLOS_MAX_RAM_PERCENTAGE=75`

* ### DEVELOPMENT
Enable development features such as external code changes (volume codegenerator), an open Java debug port at *8000*, Webclient rest explorer and some more. `-e DEVELOPMENT=true`  
For more information see *Development Environment*.

* ### KEEP_ALIVE_CHECK_TIMEOUT
Sets the timeout (in seconds) of the keep-alive check. If no response is received within this time or if the port is not in use (crash of the Java process), a shutdown/restart takes place automatically. Default is 300s. `-e KEEP_ALIVE_CHECK_TIMEOUT=300`

* ### LOCALE
Set the locale. Default is de_DE.UTF-8. `-e LOCALE=de_DE.UTF-8`

* ### TZ
Set the timezone. Default is Europe/Berlin. `-e TZ=Europe/Berlin`

* ### DB_SCHEMA
Specify the name of the schema to use in the Nuclos-DB. The schema may be used only by one Nuclos application server at the same time. Default is "nuclos". `-e DB_SCHEMA=dev`

* ### DB_CONNECTIONPOOL_SIZE
Set the database connection pool size. Default is 50. `-e DB_CONNECTIONPOOL_SIZE=50`

* ### DB_CONNECTIONPOOL_WAITSECONDS
Set the database connection pool wait time in seconds. Default is 120. `-e DB_CONNECTIONPOOL_WAITSECONDS=120`

* ### NUCLOS_CONNECTION_THREADPOOL_SIZE
Set the application server connection pool size for answering client requests. Default is 200. `-e NUCLOS_CONNECTION_THREADPOOL_SIZE=200`

* ### LIVE_SEARCH
Set Nuclos live search enabled. Default is false. `-e LIVE_SEARCH=true`

* ### CLUSTER_MODE
Starts this instance in cluster mode. Default is false `-e CLUSTER_MODE=true`
For more information visit the [Nuclos wiki](https://wiki.nuclos.de/display/Installationsanleitung/Nuclos+im+Cluster-Betrieb/)

* ### CLUSTER_NODE_TYPE
Possible values are 'master' or 'slave'. There can only be one master, but many slaves. `-e CLUSTER_NODE_TYPE=master`

* ### CLUSTER_NODE_ID
Assign a unique ID for this node. (Alphanumeric, max. 20 characters [a-zA-Z0-9]). `-e CLUSTER_NODE_ID=C3PO`

* ### CLUSTER_NODE_HOSTNAME
Other nodes can use this name to reach this instance. `-e CLUSTER_NODE_HOSTNAME=c3po.local`

* ### CLUSTER_BALANCER_PROTOCOL
Optional - Possible values are 'http' or 'https'. `-e CLUSTER_BALANCER_PROTOCOL=https`

* ### CLUSTER_BALANCER_HOSTNAME
Optional - Public hostname under which the cluster can be reached. `-e CLUSTER_BALANCER_HOSTNAME=my.nuclos.cluster`

* ### CLUSTER_BALANCER_PORT
Optional - The public port of the cluster. `-e CLUSTER_BALANCER_PORT=443`

* ### CLUSTER_BALANCER_CONTEXT
Optional - Only need to be specified if the load balancer is mapping the URL (example: 'nuclos' (the default) should be mapped to 'r2d2'). `-e CLUSTER_BALANCER_CONTEXT=r2d2`
Complete example ot CLUSTER_BALANCER_* variables in action:
Nuclos node instance (this): *http://c3po.local:8060/nuclos* 
Public cluster (Load balancer): *https://my.nuclos.cluster:443/r2d2*

* ### POSTGRES_USER
In the case of an embedded database, the default PostgreSQL user is `nuclos` with password `password`. See also [Nuclos-DB](https://hub.docker.com/r/nuccess/nuclos-db/).
You can change the default password with the file `db_password` in the volume `/opt/nuclos/secrets`.

* ### DB_MAX_RAM_PERCENTAGE
In the case of an embedded database, dynamically controls the total usable RAM for the PostgreSQL process and reduces the value of NUCLOS_MAX_RAM_PERCENTAGE. See also [Nuclos-DB](https://hub.docker.com/r/nuccess/nuclos-db/).
Default is 10%.

* ### DB_MAX_CONNECTIONS
In the case of an embedded database, set the connection limit. See also [Nuclos-DB](https://hub.docker.com/r/nuccess/nuclos-db/).
Default is 50.
