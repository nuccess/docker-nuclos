#!/bin/bash

echo "Configure Nuclos..." | tee $NUCLOS_HOME/logs/config.log

# Nuclos tomcat directory
TOMCAT_DIR=$(find $NUCLOS_HOME/tomcat -iname "apache-tomcat-*" -type d)

echo "Copy templates..." | tee -a $NUCLOS_HOME/logs/config.log
rm -f $TOMCAT_DIR/$NUCLOS_WEBCLIENT_CONF
rm -f $TOMCAT_DIR/$NUCLOS_TOMCAT_CONF
rm -f $NUCLOS_STARTUP
cp $TOMCAT_DIR/$NUCLOS_WEBCLIENT_CONF-template $TOMCAT_DIR/$NUCLOS_WEBCLIENT_CONF
cp $TOMCAT_DIR/$NUCLOS_TOMCAT_CONF-template $TOMCAT_DIR/$NUCLOS_TOMCAT_CONF
cp $NUCLOS_STARTUP-template $NUCLOS_STARTUP

# Update total memory RAM usage
echo "Set heap size..." | tee -a $NUCLOS_HOME/logs/config.log
TOTAL_RAM_GB=${TOTAL_RAM_GB:-2}
# Memory limit in bytes (64 GB = 64 * 1024 * 1024 * 1024 bytes)
LIMIT_64GB=$((64 * 1024 * 1024 * 1024))
# Read the memory limit value from the cgroup v2 file
MEM_LIMIT_BYTES=$(cat /sys/fs/cgroup/memory.max)
# Decision: Use the cgroup memory limit or the custom value
if [ "${MEM_LIMIT_BYTES:-${LIMIT_64GB}}" -lt "$LIMIT_64GB" ]; then
  # If memory is less than 64 GB, use the cgroup value
  echo "Using cgroup memory limit: $((MEM_LIMIT_BYTES / 1024 / 1024)) MB" | tee -a $NUCLOS_HOME/logs/config.log
  HEAP=$((MEM_LIMIT_BYTES / 1024 / 1024))  # Convert to MB
else
  # Otherwise, use the custom TOTAL_RAM_GB value
  echo "Using custom TOTAL_RAM_GB: ${TOTAL_RAM_GB} GB" | tee -a $NUCLOS_HOME/logs/config.log
  HEAP=$(printf "%.0f" "$(awk "BEGIN {print $TOTAL_RAM_GB * 1024}")")
fi
NUCLOS_MAX_RAM_PERCENTAGE=${NUCLOS_MAX_RAM_PERCENTAGE:-75}
MAX_RAM_PERCENTAGE=$NUCLOS_MAX_RAM_PERCENTAGE
if [ "$BUILD_TYPE" = "with-db" ]; then
  DB_MAX_RAM_PERCENTAGE=${DB_MAX_RAM_PERCENTAGE:-10}
  ADDITIONAL_LOG=" (-${DB_MAX_RAM_PERCENTAGE}% db-limit)"
  MAX_RAM_PERCENTAGE=$((NUCLOS_MAX_RAM_PERCENTAGE - DB_MAX_RAM_PERCENTAGE))
fi

# Calculate the final RAM value based on the percentage
HEAP=$(printf "%.0f" "$(awk "BEGIN {print $HEAP * $MAX_RAM_PERCENTAGE / 100}")")
echo "Total HEAP after ${NUCLOS_MAX_RAM_PERCENTAGE}% limit${ADDITIONAL_LOG}: ${HEAP} MB" | tee -a $NUCLOS_HOME/logs/config.log

sed -i 's:-'"Xmx2048"'m:-Xmx'"$HEAP"'m:g' $NUCLOS_STARTUP

export CLUSTER_NODE_PROTOCOL=http

# HTTPS keystore installation
if [ -f $NUCLOS_DIR/keystore ]; then
	echo "Set SSL..." | tee -a $NUCLOS_HOME/logs/config.log
	export CLUSTER_NODE_PROTOCOL=https
	export KEYSTORE_FILE=$NUCLOS_HOME/extra/.keystore
	cp $NUCLOS_DIR/keystore $KEYSTORE_FILE
	# Nuclos default generates warning "This server is vulnerable to a BEAST attack"
	#sed -i 's|<Connector.*/>|<Connector SSLEnabled=\"true\" clientAuth=\"false\" compressableMimeType=\"text/html,text/xml,text/js\" compression=\"on\" compressionMinSize=\"2048\" keystoreFile=\"'"$KEYSTORE_FILE"'\" keystorePass=\"'"$KEYSTORE_PASSWORD"'\" maxThreads=\"150\" port=\"80\" protocol=\"HTTP/1.1\" scheme=\"https\" secure=\"true\" sslProtocol=\"TLS\"/>|' $TOMCAT_DIR/conf/server.xml
	sed -i 's|<Connector.*/>|<Connector SSLEnabled=\"true\" clientAuth=\"false\" compressableMimeType=\"text/html,text/js,text/css,application/javascript,application/json\" compression=\"on\" compressionMinSize=\"2048\" connectionTimeout=\"20000\" keystoreFile=\"'"$KEYSTORE_FILE"'\" keystorePass=\"'"$KEYSTORE_PASSWORD"'\" maxThreads=\"'"$NUCLOS_CONNECTION_THREADPOOL_SIZE"'\" port=\"'"$NUCLOS_PORT"'\" protocol=\"HTTP/1.1\" scheme=\"https\" secure=\"true\" sslProtocol=\"TLS\" sslEnabledProtocols=\"TLSv1.2\" useServerCipherSuitesOrder=\"true\" ciphers=\"TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,TLS_DHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256,TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,TLS_DHE_RSA_WITH_AES_128_CBC_SHA256,TLS_DHE_RSA_WITH_AES_128_CBC_SHA,TLS_DHE_RSA_WITH_AES_256_CBC_SHA256,TLS_DHE_RSA_WITH_AES_256_CBC_SHA,TLS_RSA_WITH_AES_128_GCM_SHA256,TLS_RSA_WITH_AES_256_GCM_SHA384,TLS_RSA_WITH_AES_128_CBC_SHA256,TLS_RSA_WITH_AES_256_CBC_SHA256,TLS_RSA_WITH_AES_128_CBC_SHA,TLS_RSA_WITH_AES_256_CBC_SHA\"/>|' $TOMCAT_DIR/$NUCLOS_TOMCAT_CONF
	# add "-Djdk.tls.ephemeralDHKeySize=2048" to CATALINA_OPTS
	sed -i 's:JAVA_OPTS=":JAVA_OPTS="-Djdk.tls.ephemeralDHKeySize=2048 :g' $NUCLOS_STARTUP
else
	sed -i 's|<Connector.*/>|<Connector compressableMimeType=\"text/html,text/js,text/css,application/javascript,application/json\" compression=\"on\" compressionMinSize=\"2048\" connectionTimeout=\"20000\" maxThreads=\"'"$NUCLOS_CONNECTION_THREADPOOL_SIZE"'\" port=\"'"$NUCLOS_PORT"'\"/>|' $TOMCAT_DIR/$NUCLOS_TOMCAT_CONF
fi

# Install extensions
nuclos-extensions.sh

cd $NUCLOS_CONF
rm -f server.properties
cp $NUCLOS_DIR/server.properties-template server.properties

# Cluster configuration
if [ "$CLUSTER_MODE" == "true" ]; then
	echo "Enable cluster mode (Type: $CLUSTER_NODE_TYPE)..." | tee -a $NUCLOS_HOME/logs/config.log
	sed -i 's|cluster.mode=false|cluster.mode=true|g' server.properties
	# disable AutoDbSetup if it is a slave
	if [ "$CLUSTER_NODE_TYPE" == "slave" ]; then
		sed -i 's|database.autosetup=true|database.autosetup=false|g' server.properties
	fi
	# disable Lucene search (the installer does the same)
	export LIVE_SEARCH=false
	# set balancer context in config.json (Webclient)
	if [ ! "$CLUSTER_BALANCER_CONTEXT" == "nuclos" ]; then
		sed -i 's|:<port>/nuclos\"|:<port>/'"$CLUSTER_BALANCER_CONTEXT"'\"|g' "$TOMCAT_DIR/$NUCLOS_WEBCLIENT_CONF"
	fi
fi
sed -i 's|CLUSTER_NODE_TYPE|'"$CLUSTER_NODE_TYPE"'|g' server.properties
sed -i 's|CLUSTER_NODE_ID|'"$CLUSTER_NODE_ID"'|g' server.properties
# default 'http' for cluster.node.protocol (from installer) needs to be adjusted
# cluster.node.protocol=http
sed -i 's|cluster.node.protocol=http|cluster.node.protocol='"$CLUSTER_NODE_PROTOCOL"'|g' server.properties
sed -i 's|CLUSTER_NODE_HOSTNAME|'"$CLUSTER_NODE_HOSTNAME"'|g' server.properties
sed -i 's|cluster.node.port='"$NUCLOS_PORT"'|cluster.node.port='"$DOCKER_NUCLOS_PORT"'|g' server.properties
# default 'nuclos' for cluster.node.context (from installer) is OK
# cluster.node.context=nuclos
sed -i 's|CLUSTER_BALANCER_PROTOCOL|'"$CLUSTER_BALANCER_PROTOCOL"'|g' server.properties
sed -i 's|CLUSTER_BALANCER_HOSTNAME|'"$CLUSTER_BALANCER_HOSTNAME"'|g' server.properties
sed -i 's|CLUSTER_BALANCER_PORT|'"$CLUSTER_BALANCER_PORT"'|g' server.properties
sed -i 's|CLUSTER_BALANCER_CONTEXT|'"$CLUSTER_BALANCER_CONTEXT"'|g' server.properties

# Default db schema is 'nuclos'
export DB_SCHEMA=${DB_SCHEMA:-'nuclos'}

# Set db configuration
echo "Set DB connection and pool..." | tee -a $NUCLOS_HOME/logs/config.log
rm -f jdbc.properties
cp $NUCLOS_DIR/jdbc.properties-template jdbc.properties
if [ "$BUILD_TYPE" = "with-db" ]; then
	sed -i 's:DB_HOST:'"127.0.0.1"':g' jdbc.properties
else
	sed -i 's:DB_HOST:'"postgres"':g' jdbc.properties
fi
sed -i 's:DB_PORT:'"${POSTGRES_PORT_5432_TCP_PORT:-${POSTGRES_PORT}}"':g' jdbc.properties
sed -i 's:DB_DATABASE:'"${POSTGRES_ENV_POSTGRES_DB:-${POSTGRES_DB}}"':g' jdbc.properties
sed -i 's:DB_USER:'"${POSTGRES_ENV_POSTGRES_USER:-${POSTGRES_USER}}"':g' jdbc.properties
sed -i 's:DB_PASSWORD:'"${DB_PASSWORD}"':g' jdbc.properties
sed -i 's:DB_SCHEMA:'"$DB_SCHEMA"':g' server.properties
sed -i 's:DB_CONNECTIONPOOL_SIZE:'"$DB_CONNECTIONPOOL_SIZE"':g' server.properties
sed -i 's:DB_CONNECTIONPOOL_WAITSECONDS:'"$DB_CONNECTIONPOOL_WAITSECONDS"':g' server.properties

# Set live search index
if [ "$LIVE_SEARCH" == "true" ]; then
    sed -i 's:nuclos.index.path=off:'"nuclos.index.path=$NUCLOS_HOME/data/index"':g' server.properties
fi

# DEV Environment
if [ "$DEVELOPMENT" == "true" ]; then
	echo "Enable development..." | tee -a $NUCLOS_HOME/logs/config.log
	sed -i 's|JAVA_OPTS="|JAVA_OPTS="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:'"$NUCLOS_DEBUG_PORT"' |g' $NUCLOS_STARTUP
	sed -i 's|environment.development=false|environment.development=true|g' server.properties
	sed -i 's|environment.production=true|environment.production=false|g' server.properties
fi
