#!/bin/bash

while true; do
  nice -n 19 bash -c "until bash -c 'test -f /opt/nuclos/home/logs/server.log \
    && PID=\$(jps -l | grep org.apache.catalina.startup.Bootstrap | cut -d \" \" -f 1) \
    && test ! -z \$PID && JSTACK=\$(cpulimit --limit=5 --foreground --quiet -- jstack \$PID) \
    && ! test \$(echo \$JSTACK | grep \"NuclosQuartzScheduler_Worker\" | wc -l) == 0 \
    && test \$(echo \$JSTACK | grep \"AutoDbSetup\" | wc -l) == 0 \
    && echo \"Nuclos_RestartWatcher_Check_1/3\" \
    && test \$(echo \$JSTACK | grep \"Nuclet Extension Updater\" | wc -l) == 0 \
    && echo \"Nuclos_RestartWatcher_Check_2/3\" \
    && ! test \$(echo \$JSTACK | grep \"NucletServerExtensionChangedInfoThread\" | wc -l) == 0 \
    && echo \"Nuclos_RestartWatcher_Check_3/3\"' ; do sleep 15 ; done" > /dev/null 2>&1
	echo "nuclos-restart-watcher.sh" > $NUCLOS_DIR/nuclos.restart
	#echo "Restart Nuclos initiated!" >> $NUCLOS_HOME/logs/server.log
	sleep 60
done
